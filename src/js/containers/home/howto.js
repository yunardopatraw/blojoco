import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class HowTo extends Component {

    componentDidMount() {
    }

    render() {

        return (
            <section className="how-to-section">
                <span>this is How to section</span>
            </section>
        )
    }
}
function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(HowTo);