import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class CtaSubscribe extends Component {

    componentDidMount() {
    }

    render() {

        return (
            <section className="cta-subscribe-section">
                <span>this is Cta subscribe to section</span>
            </section>
        )
    }
}
function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CtaSubscribe);