import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getTransaction } from '../../actions/get-transaction';
import Router from 'next/router';
import { createTransaction } from '../../actions/create-transaction';

class ListingTransaction extends Component {

    componentDidMount() {
        this.props.getTransaction();
    }

    seeDetailClick(theDate){
        Router.push('/detail/' +  theDate);
    }

    renderAllBudgets() {

        //console.log('this.props.getTransaction', this.props.transaction);
        if (!this.props.transaction || this.props.transaction.status == 'loading') {
            return (
                <div>List loading</div>
            )
        } else {
            if (this.props.transaction.data && this.props.transaction.data.transactions && this.props.transaction.status == 'finish') {

                var uniqueDate = [...new Set(this.props.transaction.data.transactions.map(item => item.date))];

                return uniqueDate.map((theDate) => {

                    var totalAmmount = this.props.transaction.data.transactions
                        .filter(x => x.date == theDate)
                        .map( a => a.amount)
                        .reduce((a,b) => {
                            return a + b;
                        });
                    
                    return (
                        <div className='date-container' key={theDate}>
                            <div>
                                <span>date:{theDate}</span>
                            </div>
                            <div>
                                <span>total Ammount:{totalAmmount}</span>
                            </div>
                            <div className="button" onClick={() => this.seeDetailClick(theDate)}>See Detail</div>
                        </div>
                    )

                })

            }
        }
    }

    render() {

        var budgetName = (this.props.transaction && this.props.transaction.status == 'finish') ? this.props.transaction.data.name : "My Budget";

        return (
            <section className="listing">
                <h2>EXPENSES LISTING</h2>
                <div className="budget-overview">
                    {this.renderAllBudgets()}
                </div>
            </section>
        )
    }
}
function mapStateToProps(state) {
    return {
        transaction: state.transaction,
        account: state.account
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getTransaction: getTransaction,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ListingTransaction);