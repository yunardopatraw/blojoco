import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class HeroInput extends Component {

    componentDidMount() {
    }

    render() {

        return (
            <section className="hero-input-section">
                <span>this is hero input section</span>
            </section>
        )
    }
}
function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(HeroInput);