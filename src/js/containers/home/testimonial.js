import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class Testimonial extends Component {

    componentDidMount() {
    }

    render() {

        return (
            <section className="how-to-section">
                <span>this is testimonial section</span>
            </section>
        )
    }
}
function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Testimonial);