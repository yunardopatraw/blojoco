import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Header from './_header';
import Footer from './_footer';
import '../../styles/style.scss';
import Head from 'next/head'

class Layout extends Component {

	render() {
		//console.log('this.props.initdata', this.props.initdata)
		var language = this.props.initdata && this.props.initdata.language ? this.props.initdata.language :'en';
		var title = this.props.initdata && this.props.initdata.title ? this.props.initdata.title : 'Expense Control'
		return (
			<div>
				<Head>
					<script src='https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.23.0/polyfill.min.js' />
					<meta httpEquiv="content-type" content="text/html; charSet=UTF-8" />
					<meta name="verification" content="882bad4e207397d1f8792e38b1fa5083" />
					<meta httpEquiv="X-UA-Compatible" content="IE=Edge" />
					<meta charSet="UTF-8" />
					<meta httpEquiv="content-language" content={language}></meta>
					<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Roboto+Mono:100,100i,300,300i,400,400i,500,500i,700,700i|Work+Sans:100,200,300,400,500,600,700,800,900|Yellowtail"
						rel="stylesheet" />
					<title>{title}</title>
					<link rel="icon" href="/favicon.ico" />
					<link href="/manifest.json" rel="manifest" />

				</Head>
				<div id="layout">
					<Header></Header>
					{this.props.children}
					<Footer></Footer>
				</div>
			</div>

		)
	}

}

function mapStateToProps(state) {
	return {
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({
	},
		dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
