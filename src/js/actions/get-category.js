import * as ynab from 'ynab';
import WebSettings from '../../../config/web/facade/web-settings';
import CommonFunction from '../../../common/facade/common-function';

export function getCategory() {

    const accessToken = WebSettings.ynap_access_token
    const ynabAPI = new ynab.API(accessToken);

    //console.log(theState);
    return (dispatch) => {
        dispatch(CommonFunction.dispatchData('loading', null, 'GET_CATEGORY'))

        try {
            return ynabAPI.budgets.getBudgets()
                .then((budgetsResponse) => {
                    if (budgetsResponse.data.budgets && budgetsResponse.data.budgets.length > 0) {

                        var budget = budgetsResponse.data.budgets[0]
                        return ynabAPI.categories.getCategories(budget.id)
                            .then((categoryResponse) => {
                                //console.log('categoryResponse', categoryResponse.data);
                                return dispatch(CommonFunction.dispatchData('finish', categoryResponse.data, 'GET_CATEGORY'))
                            })
                    }
                }).catch(err => {
                    return dispatch(CommonFunction.dispatchData('error', err, 'GET_CATEGORY'))
                })
        } catch (error) {
            return dispatch(CommonFunction.dispatchData('error', error, 'GET_CATEGORY'))
        }
    }
}