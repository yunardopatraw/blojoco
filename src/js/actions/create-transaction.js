import * as ynab from 'ynab';
import WebSettings from '../../../config/web/facade/web-settings';
import {getTransaction} from './get-transaction';
import CommonFunction from '../../../common/facade/common-function';

export function createTransaction(budgetId, accountId, dateInput, amountInput, categoryId, memoInput) {

    const accessToken = WebSettings.ynap_access_token
    const ynabAPI = new ynab.API(accessToken);

    //console.log(theState);
    return (dispatch) => {
        dispatch(CommonFunction.dispatchData('loading', null, 'CREATE_TRANSACTION'))
        var transaction = {
            account_id: accountId,
            date: dateInput,
            amount: Math.abs(amountInput * 1000) * -1,
            category_id: categoryId,
            cleared: ynab.SaveTransaction.ClearedEnum.Cleared,
            approved: true,
            memo: memoInput,
            payee_id: null,
        }
        
        try {
            //console.log('masukin budgetId', budgetId);
            //console.log('masukin transaksi', transaction);
            return ynabAPI.transactions.createTransaction(budgetId, {transaction})
                .then((createTransactionResponse) => {
                    //console.log('budgetsResponse', createTransactionResponse.data);
                    dispatch(CommonFunction.dispatchData('need-reload', null, 'GET_TRANSACTION'))
                    return dispatch(CommonFunction.dispatchData('finish', createTransactionResponse, 'CREATE_TRANSACTION'))
                }).catch(err => {
                    console.log('err', err);
                    return dispatch(CommonFunction.dispatchData('error', err, 'CREATE_TRANSACTION'))
                })
        } catch (error) {
            console.log('error', error);
            return dispatch(CommonFunction.dispatchData('error', error, 'CREATE_TRANSACTION'))
        }
    }
}
