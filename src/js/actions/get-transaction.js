import * as ynab from 'ynab';
import WebSettings from '../../../config/web/facade/web-settings';
import CommonFunction from '../../../common/facade/common-function';

export function getTransaction() {

    const accessToken = WebSettings.ynap_access_token
    const ynabAPI = new ynab.API(accessToken);

    //console.log(theState);
    return (dispatch) => {
        dispatch(CommonFunction.dispatchData('', {}, 'CREATE_TRANSACTION'))
        dispatch(CommonFunction.dispatchData('loading', null, 'GET_TRANSACTION'));

        try {
            return ynabAPI.budgets.getBudgets()
                .then((budgetsResponse) => {
                    if (budgetsResponse.data.budgets && budgetsResponse.data.budgets.length > 0) {

                        var budget = budgetsResponse.data.budgets[0]
                        return ynabAPI.transactions.getTransactions(budget.id)
                            .then((transactionResponse) => {
                                var budgetItem = {
                                    name: budget.name,
                                    id: budget.id,
                                    currency: budget.currency_format.iso_code,
                                    transactions: transactionResponse.data.transactions,
                                }
                                return dispatch(CommonFunction.dispatchData('finish', budgetItem, 'GET_TRANSACTION'))
                            })
                    }
                }).catch(err => {
                    return dispatch(CommonFunction.dispatchData('error', err, 'GET_TRANSACTION'))
                })
        } catch (error) {
            return dispatch(CommonFunction.dispatchData('error', error, 'GET_TRANSACTION'))
        }
    }
}