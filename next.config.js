const fs = require('fs')
const {join} = require('path')
const {promisify} = require('util')
const copyFile = promisify(fs.copyFile)
const withSass = require('@zeit/next-sass')
const withCSS = require('@zeit/next-css')

module.exports = (phase, {
    defaultConfig
}) => {
    return withCSS(withSass({
        exportPathMap: async function (defaultPathMap, {
            dev,
            dir,
            outDir,
            distDir,
            buildId
        }) {
            defaultPathMap = {
                '/': {page: '/'},
            }
            if (dev) {
                return defaultPathMap
            }

            await copyFile(join(dir, '/public/static/static-file/robots.txt'), join(outDir, 'robots.txt'))
            await copyFile(join(dir, '/public/static/static-file/sitemap.xml'), join(outDir, 'sitemap.xml'))
            await copyFile(join(dir, '/public/static/static-file/favicon.ico'), join(outDir, 'favicon.ico'))
            await copyFile(join(dir, '/public/static/static-file/manifest.json'), join(outDir, 'manifest.json'))
            return defaultPathMap
        } 
    }, {
        webpack(config, options) {
            console.log('webpackCfg', config);
            console.log('webpackOpt', options);
            return config
        }
    }))
}