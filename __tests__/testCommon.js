import WebSettings from '../config/web/web-settings';
import CommonFunction from '../common/facade/common-function'

test('config has ynap token', () => {
    //console.log('WebSettings', WebSettings);
    expect(WebSettings.ynap_access_token).not.toBe('');
});

test('common function dispatchData', () => {
    var realResult = CommonFunction.dispatchData('testStatus', 'testValue', 'testType');
    expect(realResult.type).toBe('testType');
});