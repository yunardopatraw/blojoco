import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { getTransaction } from '../src/js/actions/get-transaction';

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('async actions', () => {
    let store;

    beforeEach(() => {
        store = mockStore({})
    })

    it('Check Get trasation to return something', () => {

        var expectObject = {
            type: "GET_TRANSACTION",
            payload: expect.anything()
        }

        return store.dispatch(getTransaction())
            .then(() => {
                expect(store.getActions()).toContainEqual(expect.objectContaining(expectObject))
            })
    })


})