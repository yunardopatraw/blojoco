const CommonFunction = {

    dispatchData(status, value, type) {
        return {
            type: type,
            payload: {
                status: status,
                data: value
            }
        }
    }
}

module.exports = Object.assign({ CommonFunction })
