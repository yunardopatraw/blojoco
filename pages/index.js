import React from 'react';
import {withRouter} from 'next/router';
import {Provider} from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import allReducers from '../src/js/reducers/index';
import Layout from '../src/js/components/_layout';
import Hero from '../src/js/containers/home/hero';
import HowTo from '../src/js/containers/home/howto';
import Testimonial from '../src/js/containers/home/testimonial';
import WhyUs from '../src/js/containers/home/why-us';
import CtaSubscribe from '../src/js/containers/home/cta-subscribe';

const middleware = applyMiddleware(logger, thunk);
const store = createStore(allReducers, middleware);

const Home = withRouter((props) => {
    return (
        <Provider store={store}>
            <Layout initdata={props.initdata}>
                <Hero></Hero>
                <HowTo></HowTo>
                <Testimonial></Testimonial>
                <WhyUs></WhyUs>
                <CtaSubscribe></CtaSubscribe>
            </Layout>
        </Provider>
    )
});

Home.getInitialProps = async function(context) {
    
    var data = {};
    data.language = context.query.lang ? context.query.lang : 'en'
    data.title = "Expense Control"

	return {
		initdata: data
	}
}

export default Home;