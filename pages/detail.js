import React from 'react';
import {withRouter} from 'next/router';
import {Provider} from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import allReducers from '../src/js/reducers/index';
import Layout from '../src/js/components/_layout';
import DetailView from '../src/js/containers/detail/detail-view';

const middleware = applyMiddleware(logger, thunk);
const store = createStore(allReducers, middleware);

const Detail = withRouter((props) => {
    return (
        <Provider store={store}>
            <Layout initdata={props.initdata}>
                <DetailView initdata={props.initdata}></DetailView>
            </Layout>
        </Provider>
    )
});

Detail.getInitialProps = async function(context) {
    
    var data = {};
    data.language = context.query.lang ? context.query.lang : 'en'
    data.title = "Expense Control " + context.query.currentdate;
    data.currentdate = context.query.data;

	return {
		initdata: data
	}
}

export default Detail;