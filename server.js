const express = require('express')
const next = require('next')
const {join} = require('path')
const LRUCache = require('lru-cache')

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({dev});
const handle = app.getRequestHandler();
const compression = require('compression');


const ssrCache = new LRUCache({
	length: function (n, key) {
	  return n.toString().length + key.toString().length
	},
	max: 1000 * 1000 * 1000, // 1000MB cache soft limit
	maxAge: 1 * 24 * 1000 * 60 * 60 // 1 day
})

app.prepare().then(() => {

	const server = express()
	server.use(compression()) 

	SetStaticContent(server);

	RegisterDetailPage(server);

	RegisterGeneric(server);

	server.listen(port, err => {
		if (err) {
			console.error('listening error', {'err': err.message});
		}

		console.log(`> Ready on http://localhost:${port}`)
	})

})

function RegisterDetailPage(server) {
	server.get('/:lang/detail/:data', (req, res) => {

		if (req.params.data.length != 10){
			res.redirect(301, '/en');
		} else {
			const actualPage = '/detail';
			const queryParams = {
				data: req.params.data,
				lang: req.params.lang,
			};
			return app.render(req, res, actualPage, queryParams)
		}
	});
	server.get('/detail/:data', (req, res) => {
		res.redirect(301, '/en/detail/' +  req.params.data);
	});
}

function RegisterGeneric(server) {
	
	server.get('/:lang', (req, res) => {
		const actualPage = '/index';
		const queryParams = {
			lang: req.params.lang,
			amp: req.query.amp ? req.query.amp[0] == 1 : false
		};

		if (queryParams.lang.length > 2){
			return handle(req, res);
		}

		return app.render(req, res, actualPage, queryParams)
	});
	server.get('/', (req, res) => {
		res.redirect(301, '/en/');
	});
	server.get('*', (req, res) => {
		return handle(req, res);
	});
}

function SetStaticContent(server) {

	server.get('/favicon.ico', (req, res) => {
		const path = join(__dirname, 'public/static/static-file/favicon.ico');
		return app.serveStatic(req, res, path);
	});
	server.get('/manifest.json', (req, res) => {
		const path = join(__dirname, 'public/static/static-file/manifest.json');
		return app.serveStatic(req, res, path);
	});

}

