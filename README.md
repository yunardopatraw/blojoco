## Getting Started

To get started, first install all the necessary dependencies.
```
> npm install
```

Development Mode
```
> npm run dev-serve
```

Deployment Mode
```
> npm run build
> npm run start-serve
```

## Docker Run

Run the code using babel:

Build Docker
```
> docker build -t expense-control .
```

Run Docker
```
> docker run -p 3001:3000 expense-control
```

Browse from website
```
> http://localhost:3001/
```


To view your project, go to: [http://localhost:3000/](http://localhost:3000/)

